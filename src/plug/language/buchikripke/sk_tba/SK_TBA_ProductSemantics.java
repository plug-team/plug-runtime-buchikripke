package plug.language.buchikripke.sk_tba;

import plug.core.*;
import plug.language.buchi.runtime.BuchiConfiguration;
import plug.language.buchi.runtime.BuchiRuntime;
import plug.language.buchikripke.runtime.KripkeBuchiAtomicPropositionsEvaluator;
import plug.language.buchikripke.runtime.KripkeBuchiConfiguration;
import plug.language.buchikripke.runtime.KripkeBuchiProductSemantics;
import plug.statespace.transitions.FiredTransition;
import plug.utils.Pair;
import properties.BuchiAutomata.BuchiAutomataModel.GuardedTransition;
import properties.PropositionalLogic.interpreter.Evaluator;
import properties.PropositionalLogic.interpreter.EvaluatorSpaghetti;
import properties.PropositionalLogic.interpreter.atom.AtomArrayValuationEvaluator;

import java.util.*;

public class SK_TBA_ProductSemantics implements ITransitionRelation<KripkeBuchiConfiguration, Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> {
    protected final RuntimeDescription kripke;
    protected final RuntimeDescription buchi;

    protected ITransitionRelation<IConfiguration, Object> kripkeRuntime;
    protected BuchiRuntime buchiRuntime;
    protected KripkeBuchiAtomicPropositionsEvaluator atomicPropositionsEvaluator = null;

    protected final EvaluatorSpaghetti evaluator = new EvaluatorSpaghetti();

    protected final AtomArrayValuationEvaluator apValuationEvaluator;

    boolean hasDeadlockAP = false;

    public SK_TBA_ProductSemantics(RuntimeDescription kripke, RuntimeDescription buchi) throws Exception {
        this.kripke = kripke;
        this.buchi = buchi;

        ITransitionRelation runtime = buchi.getRuntime();
        buchiRuntime = runtime instanceof BuchiRuntime ? (BuchiRuntime) runtime : null;

        kripkeRuntime = kripke.getRuntime();

        //TODO: HACK: the AtomArray should be set at loading the next line is a test
        Map<String, Integer> atomMap = new HashMap<>();
        int idx = 0;
        for (String atom : getBuchiRuntime().getAtomicPropositions()) {
            if (atom.equals("deadlock")) {
                hasDeadlockAP = true;
                atomMap.put(atom, idx++);
                break;
            }
        }

        List<String> atoms = new ArrayList<>();
        for (String atom : getBuchiRuntime().getAtomicPropositions()) {
            if (atom != null && !atomMap.containsKey(atom)) {
                atomMap.put(atom, idx++);
                atoms.add(atom);
            }
        }

        IAtomicPropositionsEvaluator kripkeEvaluator = getKripkeRuntime().getAtomicPropositionEvaluator();
        kripkeEvaluator.registerAtomicPropositions(atoms.toArray(new String[0]));
        apValuationEvaluator = new AtomArrayValuationEvaluator(atomMap);
        evaluator.setDefaultEvaluator(apValuationEvaluator);
        //HACK: until here
    }

    public SK_TBA_ProductSemantics(RuntimeDescription kripke, BuchiRuntime buchiRuntime) throws Exception {
        this(kripke, new RuntimeDescription(null, () -> buchiRuntime));
    }


    public RuntimeDescription getKripke() {
        return kripke;
    }

    public RuntimeDescription getBuchi() {
        return buchi;
    }

    protected ITransitionRelation<IConfiguration, Object> getKripkeRuntime() {
        return kripkeRuntime;
    }

    protected BuchiRuntime getBuchiRuntime() {
        return buchiRuntime;
    }

    @Override
    public Set<KripkeBuchiConfiguration> initialConfigurations() {
        Set<KripkeBuchiConfiguration> result = new HashSet<>();
        for (IConfiguration cKripke : getKripkeRuntime().initialConfigurations()) {
            for (IBuchiConfiguration cBuchi : getBuchiRuntime().initialConfigurations()) {
                KripkeBuchiConfiguration kbc = new KripkeBuchiConfiguration();
                kbc.kripke = cKripke;
                kbc.buchiState = cBuchi;
                result.add(kbc);
            }
        }

        return result;
    }

    @Override
    public Collection<Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> fireableTransitionsFrom(KripkeBuchiConfiguration source) {
        Collection<Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> synchronousFireables = new ArrayList<>();

        //get all fireables of the first operand (the kripke structure)
        Collection<Object> kFireableTransitions = getKripkeRuntime().fireableTransitionsFrom(source.kripke);
        int numberOfFireableTransitions = kFireableTransitions.size();

        for (Object kripkeFireable : kFireableTransitions) {
            IFiredTransition<IConfiguration, ?> kripkeTransition = getKripkeRuntime().fireOneTransition(source.kripke, kripkeFireable);

            //if the kripkeTransition is null or no targets then it means that the transition was blocked during firing.
            if (kripkeTransition == null || kripkeTransition.getTargets() == null || kripkeTransition.getTargets().isEmpty()) {
                if (getKripkeRuntime().hasBlockingTransitions()) {
                    numberOfFireableTransitions--;
                    continue;
                } else {
                    throw new RuntimeException("The runtime " + getKripkeRuntime()
                            + " does not have blocking transitions, yet it didn't produce a target when firing "
                            + kripkeFireable + " in " + source.kripke);
                }
            }

            getKripkeBuchiSynchronousTransitions(
                    source,
                    kripkeTransition,
                    synchronousFireables);
        }

        if (numberOfFireableTransitions == 0) {
            //the stuttering step because Kripke is deadlock
            getKripkeBuchiSynchronousTransitions(
                    source,
                    KripkeBuchiProductSemantics.StutteringTransition.instance,
                    synchronousFireables);
        }

        return synchronousFireables;
    }

    @Override
    public IAtomicPropositionsEvaluator<KripkeBuchiConfiguration, Object> getAtomicPropositionEvaluator() {
        if (atomicPropositionsEvaluator == null) {
            atomicPropositionsEvaluator = new KripkeBuchiAtomicPropositionsEvaluator(getKripkeRuntime().getAtomicPropositionEvaluator());
        }
        return atomicPropositionsEvaluator;
    }

    @Override
    public IFiredTransition<KripkeBuchiConfiguration, ?> fireOneTransition(
            KripkeBuchiConfiguration source,
            Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition> transition
    ) {
        KripkeBuchiConfiguration myNewState = source.createCopy();

        myNewState.kripke = transition.a.getTargets().iterator().next(); //only one target possible here
        IFiredTransition<? extends IBuchiConfiguration, ?> buchiTransition = getBuchiRuntime().fireOneTransition((BuchiConfiguration) source.buchiState, transition.b);
        myNewState.buchiState = buchiTransition.getTarget(0);

        return new FiredTransition<>(source, myNewState, transition);
    }

    @Override
    public void close() {
        getKripkeRuntime().close();
    }

    //return the Pairs of (Kripke, Buchi) transitions that can be fired synchronously
    void getKripkeBuchiSynchronousTransitions(
            KripkeBuchiConfiguration source,
            Object kripkeFiredTransition,
            Collection<Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> outFireables
    ) {
        Object kripkeFireable = null;
        Object kripkePayload = null;
        Collection<IConfiguration> kripkeTargets;

        boolean isDeadlock = false;
        if (kripkeFiredTransition == KripkeBuchiProductSemantics.StutteringTransition.instance) {
            kripkeFireable = kripkeFiredTransition;
            kripkeTargets = Collections.singleton(source.kripke);
            isDeadlock = true;
        } else {
            IFiredTransition<IConfiguration, Object> transition = ((IFiredTransition<IConfiguration, Object>) kripkeFiredTransition);
            kripkeFireable = transition.getAction();
            kripkePayload = transition.getPayload();
            kripkeTargets = transition.getTargets();
        }
        IAtomicPropositionsEvaluator<IConfiguration, Object> apEvaluator = getKripkeRuntime().getAtomicPropositionEvaluator();
        for (IConfiguration kripkeTarget : kripkeTargets) {
            synchronized (apValuationEvaluator) {

                if (!isDeadlock) {
                    boolean[] valuation = apEvaluator.getAtomicPropositionValuations(
                            source.kripke,
                            (kripkeFireable == kripkeFiredTransition ? null : kripkeFireable),
                            kripkePayload,
                            kripkeTarget);
                    if (!hasDeadlockAP) {
                        apValuationEvaluator.setValuation(valuation);
                    } else {
                        boolean[] v = new boolean[valuation.length + 1];
                        v[0] = isDeadlock; //no deadlock
                        System.arraycopy(valuation, 0, v, 1, valuation.length);
                        apValuationEvaluator.setValuation(v);
                    }
                } else {
                    //if deadlock only the deadlock AP is true
                    boolean[] valuation = new boolean[apValuationEvaluator.atomicPropositions.size()];
                    if (hasDeadlockAP) {
                        valuation[0] = true;
                    }
                    apValuationEvaluator.setValuation(valuation);
                }

                Collection<GuardedTransition> buchiTransitions = getBuchiRuntime().fireableTransitionsFrom((BuchiConfiguration) source.buchiState);
                for (GuardedTransition buchiTransition : buchiTransitions) {
                    if (evaluator.evaluate(buchiTransition.getGuard())) {
                        outFireables.add(new Pair<>(new FiredTransition<>(source.kripke, kripkeTarget, kripkeFireable), buchiTransition));
                    }
                }
            }
        }
    }
}
