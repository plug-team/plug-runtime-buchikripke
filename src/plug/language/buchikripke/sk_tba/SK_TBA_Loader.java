package plug.language.buchikripke.sk_tba;

import com.fasterxml.jackson.databind.ObjectMapper;
import plug.core.ILanguageLoader;
import plug.core.RuntimeDescription;
import plug.language.buchi.runtime.BuchiRuntime;
import plug.language.buchikripke.runtime.KripkeBuchiDescription;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.LTL.parser.Parser;
import properties.LTL.transformations.LTL2Buchi;
import properties.PropositionalLogic.PropositionalLogicModel.DeclarationBlock;
import properties.PropositionalLogic.PropositionalLogicModel.Expression;

import java.io.PrintWriter;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class SK_TBA_Loader implements ILanguageLoader<SK_TBA_ProductSemantics> {

    @Override
    public SK_TBA_ProductSemantics getRuntime(URI modelURI, Map<String, Object> options) throws Exception {
        KripkeBuchiDescription description = new ObjectMapper().readValue(modelURI.toURL(), KripkeBuchiDescription.class);

        Path modelPath = Paths.get(description.getModel());
        Path resolvedModelPath = modelPath.isAbsolute() ? modelPath : Paths.get(modelURI).getParent().resolve(modelPath);
        RuntimeDescription kripke = new RuntimeDescription(resolvedModelPath);

        StringBuilder ltl = new StringBuilder();
        description.getProperties().forEach((name, expression)-> {
            ltl.append(name);
            ltl.append(" = ");
            ltl.append(expression);
            ltl.append("\n");
        });
        BuchiDeclaration buchiAutomaton = new LTL2Buchi(new PrintWriter(System.out)).getBuchiDeclaration(ltl.toString());
        BuchiRuntime buchiRuntime = new BuchiRuntime(buchiAutomaton);
        SK_TBA_ProductSemantics kbProductSemantics = new SK_TBA_ProductSemantics(kripke, buchiRuntime);
        return kbProductSemantics;
    }
}
