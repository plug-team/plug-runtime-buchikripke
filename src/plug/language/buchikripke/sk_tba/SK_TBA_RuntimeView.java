package plug.language.buchikripke.sk_tba;

import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.core.IRuntimeView;
import plug.core.view.ConfigurationItem;
import plug.language.buchi.runtime.BuchiRuntimeView;
import plug.language.buchikripke.runtime.KripkeBuchiConfiguration;

import plug.language.buchikripke.runtime.KripkeBuchiProductSemantics;
import plug.utils.Pair;
import properties.BuchiAutomata.BuchiAutomataModel.GuardedTransition;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SK_TBA_RuntimeView implements IRuntimeView<KripkeBuchiConfiguration, Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> {

	protected final SK_TBA_ProductSemantics runtime;

	final IRuntimeView kripkeRuntimeView;
	final IRuntimeView buchiRuntimeView;

	public SK_TBA_RuntimeView(SK_TBA_ProductSemantics runtime) {
		this.runtime = runtime;
		this.kripkeRuntimeView = runtime.getKripke().getModule().getRuntimeView(runtime.getKripkeRuntime());
		this.buchiRuntimeView = new BuchiRuntimeView(runtime.getBuchiRuntime());
	}

	@Override
	public SK_TBA_ProductSemantics getRuntime() {
		return runtime;
	}

	@Override
	public List<ConfigurationItem> getConfigurationItems(KripkeBuchiConfiguration value) {
		List<ConfigurationItem> children = new ArrayList<>();

		// creates buchi description
		List<ConfigurationItem> buchiState = buchiRuntimeView.getConfigurationItems(value.buchiState);
		ConfigurationItem buchiProcess = new ConfigurationItem("process", "bucchi", null, buchiState);
		buchiProcess.setExpanded(true);
		children.add(buchiProcess);

		// create kripke description
		if (value.kripke != null) {
			String description = kripkeRuntimeView.getConfigurationDescription(value.kripke);
			List<ConfigurationItem> kripkeChildren = kripkeRuntimeView.getConfigurationItems(value.kripke);
			ConfigurationItem kripkeProcess = new ConfigurationItem("process", description, null, kripkeChildren);
			kripkeProcess.setExpanded(true);
			children.add(kripkeProcess);
		} else {
			children.add(new ConfigurationItem("process", "<not present>", null, null));
		}
		return children;
	}

	@Override
	public String getFireableTransitionDescription(Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition> transition) {
		//for the kripke the fireable is the action of the fired
		Object action = transition.a.getAction();
		if (action instanceof KripkeBuchiProductSemantics.StutteringTransition) {
			return "Buchi stuttering";
		} else if (action == null) {
			return "Buchi initial transition";
		} else {
			String kripkeTransitionDescription = kripkeRuntimeView.getFireableTransitionDescription(action);
			String buchiTransitionDescription = buchiRuntimeView.getFireableTransitionDescription(transition.b);
			return kripkeTransitionDescription + "\n/\\\n" + buchiTransitionDescription;
		}
	}

	@Override
	public String getActionDescription(Object action) {
		if (action instanceof Pair) {
			Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition> transition = (Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>) action;
			Object childAction = transition.a.getAction();
			if (childAction instanceof KripkeBuchiProductSemantics.StutteringTransition) {
				return "Buchi stuttering";
			} else if (childAction == null) {
				return "Buchi initial transition";
			} else {
				return kripkeRuntimeView.getActionDescription(childAction);
			}
		} else {
			return Objects.toString(action);
		}
	}
}
