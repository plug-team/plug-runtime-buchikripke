package plug.language.buchikripke.sk_tba;

import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.core.execution.ControllerProviderFunction;
import plug.explorer.buchi.nested_dfs.BA_GaiserSchwoon_Iterative;

import java.util.Map;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class SK_TBA_Plugin implements ILanguagePlugin<SK_TBA_ProductSemantics> {
    private final SK_TBA_Loader loader = new SK_TBA_Loader();

    @Override
    public String[] getExtensions() {
        return new String[] {".buchi"};
    }

    @Override
    public String getName() {
        return "Buchi";
    }

    @Override
    public SK_TBA_Loader getLoader() {
        return loader;
    }

    @Override
    public IRuntimeView getRuntimeView(SK_TBA_ProductSemantics runtime) {
        return new SK_TBA_RuntimeView(runtime);
    }

    @Override
    public Map<String, ControllerProviderFunction> executions() {
        Map<String, ControllerProviderFunction> executionDescriptionMap = ILanguagePlugin.defaultExecutions();
        executionDescriptionMap.put("Gaiser Schwoon [iterative]", BA_GaiserSchwoon_Iterative::new);
        return executionDescriptionMap;
    }
}
