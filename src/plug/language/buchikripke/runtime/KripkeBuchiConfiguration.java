package plug.language.buchikripke.runtime;

import plug.core.IBuchiConfiguration;
import plug.core.IConfiguration;
import plug.core.defaults.DefaultConfiguration;

public class KripkeBuchiConfiguration extends DefaultConfiguration<KripkeBuchiConfiguration> implements IBuchiConfiguration {

    public IConfiguration kripke;
    public IBuchiConfiguration buchiState;

    @Override
    public KripkeBuchiConfiguration createCopy() {
        KripkeBuchiConfiguration clone = new KripkeBuchiConfiguration();
        clone.kripke = kripke;
        clone.buchiState = buchiState;
        return clone;
    }

    @Override
    public int hashCode() {
        return (kripke != null ? kripke.hashCode() : 0) + buchiState.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if(obj instanceof KripkeBuchiConfiguration) {
            KripkeBuchiConfiguration other = (KripkeBuchiConfiguration) obj;
            return buchiState.equals(other.buchiState)
                    && ((kripke == null && kripke == other.kripke) //null object
                            || (kripke != null && kripke.equals(other.kripke)));
        }
        return false;
    }

    @Override
    public String toString() {
        return "[" + (kripke != null ? kripke.toString() : "null") + ", " + buchiState.toString() + "]";
    }

    public boolean isAccepting() {
        return buchiState.isAccepting();
    }
}
