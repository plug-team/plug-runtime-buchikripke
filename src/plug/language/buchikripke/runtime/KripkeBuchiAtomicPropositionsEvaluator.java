package plug.language.buchikripke.runtime;

import plug.core.IAtomicPropositionsEvaluator;

public class KripkeBuchiAtomicPropositionsEvaluator implements IAtomicPropositionsEvaluator<KripkeBuchiConfiguration, Object> {

	private final IAtomicPropositionsEvaluator childEvaluator;

	private int atomMaxId = 0;

	public KripkeBuchiAtomicPropositionsEvaluator(IAtomicPropositionsEvaluator childEvaluator) {
		this.childEvaluator = childEvaluator;
	}

	@Override
	public int[] registerAtomicPropositions(String[] atomicPropositions) throws Exception {
		int[] ids = childEvaluator.registerAtomicPropositions(atomicPropositions);
		for (int id : ids) {
			atomMaxId = id > atomMaxId ? id : atomMaxId;
		}
		return ids;
	}

	@Override
	public boolean[] getAtomicPropositionValuations(KripkeBuchiConfiguration configuration) {
		if (configuration.kripke != null) {
			return childEvaluator.getAtomicPropositionValuations(configuration.kripke);
		}
		return new boolean[atomMaxId+1];
	}
}
