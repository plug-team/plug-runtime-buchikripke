package plug.language.buchikripke.runtime;

import java.util.Map;

import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.core.execution.ControllerProviderFunction;
import plug.explorer.buchi.nested_dfs.BA_GaiserSchwoon_Iterative;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class KripkeBuchiPlugin implements ILanguagePlugin<KripkeBuchiProductSemantics> {
    private final KripkeBuchiLoader loader = new KripkeBuchiLoader();

    @Override
    public String[] getExtensions() {
        return new String[] {".buchi"};
    }

    @Override
    public String getName() {
        return "Buchi";
    }

    @Override
    public KripkeBuchiLoader getLoader() {
        return loader;
    }

    @Override
    public IRuntimeView getRuntimeView(KripkeBuchiProductSemantics runtime) {
        return new KripkeBuchiRuntimeView(runtime);
    }

    @Override
    public Map<String, ControllerProviderFunction> executions() {
        Map<String, ControllerProviderFunction> executionDescriptionMap = ILanguagePlugin.defaultExecutions();
        executionDescriptionMap.put("Gaiser Schwoon [iterative]", BA_GaiserSchwoon_Iterative::new);
        return executionDescriptionMap;
    }
}
