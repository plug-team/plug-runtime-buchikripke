package plug.language.buchikripke.runtime;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

public class KripkeBuchiDescription {

	// model to verify
	private final String model;

	// properties to check
	private final Map<String, String> properties;

	public KripkeBuchiDescription(
			@JsonProperty("model") String model,
			@JsonProperty("properties") Map<String, String> properties
	) {
		this.model = model;
		this.properties = properties;
	}

	public String getModel() {
		return model;
	}

	public Map<String, String> getProperties() {
		return properties;
	}
}
