package plug.language.buchikripke.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import plug.core.IAtomicPropositionsEvaluator;
import plug.core.IBuchiConfiguration;
import plug.core.IConfiguration;
import plug.core.IFiredTransition;
import plug.core.ITransitionRelation;
import plug.core.RuntimeDescription;
import plug.language.buchi.runtime.BuchiConfiguration;
import plug.language.buchi.runtime.BuchiRuntime;
import plug.statespace.transitions.FiredTransition;
import plug.utils.Pair;
import properties.BuchiAutomata.BuchiAutomataModel.GuardedTransition;
import properties.PropositionalLogic.interpreter.Evaluator;
import properties.PropositionalLogic.interpreter.EvaluatorSpaghetti;
import properties.PropositionalLogic.interpreter.atom.AtomArrayValuationEvaluator;

public class KripkeBuchiProductSemantics implements ITransitionRelation<KripkeBuchiConfiguration, Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> {

    protected final RuntimeDescription kripke;
    protected final RuntimeDescription buchi;

    protected ITransitionRelation kripkeRuntime;
    protected BuchiRuntime buchiRuntime;
    protected KripkeBuchiAtomicPropositionsEvaluator atomicPropositionsEvaluator = null;

    protected final EvaluatorSpaghetti evaluator = new EvaluatorSpaghetti();

    protected final AtomArrayValuationEvaluator apValuationEvaluator;

    public KripkeBuchiProductSemantics(RuntimeDescription kripke, RuntimeDescription buchi) throws Exception {
        this.kripke = kripke;
        this.buchi = buchi;

        ITransitionRelation runtime = buchi.getRuntime();
        buchiRuntime = runtime instanceof BuchiRuntime ? (BuchiRuntime) runtime : null;

        kripkeRuntime = kripke.getRuntime();
        
        //TODO: HACK: the AtomArray should be set at loading the next line is a test
        List<String> atoms = new ArrayList<>();
        Map<String, Integer> atomMap = new HashMap<>();
        int idx = 0;
        for (String atom : getBuchiRuntime().getAtomicPropositions()) {
            if (atom != null && !atomMap.containsKey(atom)) {
            	atomMap.put(atom, idx++);
                atoms.add(atom);
            }
        }

        IAtomicPropositionsEvaluator kripkeEvaluator = getKripkeRuntime().getAtomicPropositionEvaluator();
        kripkeEvaluator.registerAtomicPropositions(atoms.toArray(new String[0]));
        apValuationEvaluator = new AtomArrayValuationEvaluator(atomMap);
        evaluator.setDefaultEvaluator(apValuationEvaluator);
        //HACK: until here
    }

    public KripkeBuchiProductSemantics(RuntimeDescription kripke, BuchiRuntime buchiRuntime) throws Exception {
        this(kripke, new RuntimeDescription(null, () -> buchiRuntime));
    }


    public RuntimeDescription getKripke() {
        return kripke;
    }

    public RuntimeDescription getBuchi() {
        return buchi;
    }

    protected ITransitionRelation getKripkeRuntime() {
        return kripkeRuntime;
    }

    protected BuchiRuntime getBuchiRuntime() {
        return buchiRuntime;
    }

    @Override
    public Set<KripkeBuchiConfiguration> initialConfigurations() {
        Set<? extends IBuchiConfiguration> buchiInitialSet = getBuchiRuntime().initialConfigurations();

        Set<KripkeBuchiConfiguration> result = new HashSet<>();

        for (IBuchiConfiguration b : buchiInitialSet) {
            KripkeBuchiConfiguration kbc = new KripkeBuchiConfiguration();
            kbc.kripke = null; //when transforming kripe to buchi we need a new initial state. null is good enough
            kbc.buchiState = b;
            result.add(kbc);
        }

        return result;
    }

    @Override
    public Collection<Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> fireableTransitionsFrom(KripkeBuchiConfiguration source) {
        Collection<Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> fireables = new ArrayList<>();
        //if the source.kripke is null, it means that we are in the initial state
        if (source.kripke == null) {
            getKripkeBuchiSynchronousTransitions(source, getKripkeRuntime().initialConfigurations(), null, fireables);
            return fireables;
        }

        Collection<Object> fireableTransitions = getKripkeRuntime().fireableTransitionsFrom(source.kripke);
        int numberOfFireableTransitions = fireableTransitions.size();
        for (Object kripkeFireable : fireableTransitions) {
            IFiredTransition<IConfiguration, ?> kripkeTransition = getKripkeRuntime().fireOneTransition(source.kripke, kripkeFireable);
            //if the kripkeTransition is null then it means that the transition was blocked during firing.
            if (kripkeTransition == null && getKripkeRuntime().hasBlockingTransitions()) {
                numberOfFireableTransitions--;
                continue;
            }

            getKripkeBuchiSynchronousTransitions(source, kripkeTransition.getTargets(), kripkeTransition.getAction(), fireables);
        }

        if (numberOfFireableTransitions == 0) {
            //the stuttering step because Kripke is deadlock
            getKripkeBuchiSynchronousTransitions(source, Collections.singleton(source.kripke), StutteringTransition.instance, fireables);
        }

        return fireables;
    }

    @Override
    public IAtomicPropositionsEvaluator<KripkeBuchiConfiguration, Object> getAtomicPropositionEvaluator() {
        if (atomicPropositionsEvaluator == null) {
            atomicPropositionsEvaluator = new KripkeBuchiAtomicPropositionsEvaluator(getKripkeRuntime().getAtomicPropositionEvaluator());
        }
        return atomicPropositionsEvaluator;
    }

    public static class StutteringTransition { public static StutteringTransition instance = new StutteringTransition(); }

    @Override
    public IFiredTransition<KripkeBuchiConfiguration, ?> fireOneTransition(
            KripkeBuchiConfiguration source,
            Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition> transition
    ) {
        KripkeBuchiConfiguration myNewState = source.createCopy();

        myNewState.kripke = transition.a.getTargets().iterator().next(); //only one target possible here
        IFiredTransition<? extends IBuchiConfiguration, ?> buchiTransition = getBuchiRuntime().fireOneTransition((BuchiConfiguration) source.buchiState, transition.b);
        myNewState.buchiState = buchiTransition.getTarget(0);

        return new FiredTransition<>(source, myNewState, transition);
    }

    @Override
    public void close() {
        getKripkeRuntime().close();
    }

    //return the Pairs of (Kripke, Buchi) transitions that can be fired synchronously
    void getKripkeBuchiSynchronousTransitions(
            KripkeBuchiConfiguration source,
            Collection<IConfiguration> kripkeTargets,
            Object kripkeFireable,
            Collection<Pair<IFiredTransition<IConfiguration, Object>, GuardedTransition>> outFireables
    ) {
        IAtomicPropositionsEvaluator kripkeEvaluator = getKripkeRuntime().getAtomicPropositionEvaluator();
        for (IConfiguration target : kripkeTargets) {
            synchronized (apValuationEvaluator) {
                apValuationEvaluator.setValuation(kripkeEvaluator.getAtomicPropositionValuations(target));

                Collection<GuardedTransition> buchiTransitions = getBuchiRuntime().fireableTransitionsFrom((BuchiConfiguration) source.buchiState);
                for (GuardedTransition transition : buchiTransitions) {
                    if (evaluator.evaluate(transition.getGuard())) {
                        outFireables.add(new Pair<>(new FiredTransition<>(source.kripke, target, kripkeFireable), transition));
                    }
                }
            }
        }
    }
}
